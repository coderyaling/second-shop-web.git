Component({
  data: {
    isShow: false,
    currentTab: 0, // 默认首页为选中页面
    selected: 0,
    userType: '1',
    userId: '',
    color: "#9f9f9f",
    isClueShow: null,
    selectedColor: "#53C3C4",
    "list": [{
        "pagePath": "/pages/index/index",
        "iconPath": "../icons/home.png",
        "selectedIconPath": "../icons/home-active.png",
        "text": "首页"
      },
      {
        "pagePath": "/pages/publish/publish",
        "iconPath": "../icons/publish.png",
        "selectedIconPath": "../icons/publish-active.png",
        "text": "发布"
      },
      {
        "pagePath": "/pages/my/my",
        "iconPath": "../icons/my.png",
        "selectedIconPath": "../icons/my-active.png",
        "text": "我的"
      }
    ]
  },
  attached() {},
  methods: {
    // 路由跳转
    switchTab(e) {
      const data = e.currentTarget.dataset
      var index = data.index
      const url = data.path
      wx.switchTab({
        url
      })
    },
  }
})