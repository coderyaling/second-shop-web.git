// pages/transferor/transferor.js
const util = require('../../request/index')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    transferorList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  async getTransferorList() {
   var transferorList = wx.getStorageSync('transferorList')
   if (transferorList) {
     this.setData({
       transferorList
     })
   } else {
    util.request_get('/web/order/seller/on-sold', {
      userStoreId: '22080000'
    }).then((res) => {
      if (res.code === 200) {
        // 下次报错的话  先看看你响应结果有没有成功
        console.log(res); // 这里不是打印了吗 
        this.setData({
          transferorList: res.orderList
        })
        wx.setStorageSync('transferorList', res.orderList)
      }
    })
   }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
this.getTransferorList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})