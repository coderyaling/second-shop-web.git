const util = require('../../request/index')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    messageInfo: {},
    showModal: false
  },
 
  headimage: function () {

    var  _this = this;

     wx.chooseImage({

       count: 1, // 默认9     

       sizeType: ['original', 'compressed'],

      // 指定是原图还是压缩图，默认两个都有     

       sourceType: ['album', 'camera'],

      // 指定来源是相册还是相机，默认两个都有   

       success: function (res) {   

         _this.setData({

           head: res.tempFilePaths

        })

      }

    })

  },
  async getMessageInfo() {
    var userInfo = wx.getStorageSync('userInfo')
    if (userInfo) {
      this.setData({
        messageInfo: userInfo
      })
    } else {
      util.request_get('/web/user/personal-info', {
        userId: '22080000'
      }).then((res) => {
        if (res.code == 200) {
          // 下次报错的话  先看看你响应结果有没有成功 
          this.setData({
            messageInfo: res.userInfo
          })
          wx.setStorageSync('userInfo', res.userInfo)
        }
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getMessageInfo();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})