// pages/store/store.js
const util = require('../../request/index')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    storeList:[],
    userAvatar: ""
  },
  // previewImg:function(e){
  //   console.log(1);
  //   var current = this.data.images
  //   wx.previewImage({
  //     current: current,        //这里是前点击的查看的第一张图
  //     urls:this.data.imgList   //这里就是存档多图的数组放大可轮播
  //   })
  //  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
this.getStoreList();
  },
  async getStoreList() {
    var userInfo = wx.getStorageSync('userInfo');
    if (userInfo) {
      console.log(userInfo);
      var storeList = wx.getStorageSync('storeList');
    if (storeList) {
      this.setData({
        storeList:storeList,
        userAvatar: userInfo.userAvatar
      })
    } else {
      util.request_get('/web/user/store/goods-list', {
        userId: userInfo.userId
      }).then((res) => {
        if (res.code === 200) {
          this.setData({
            storeList: res.data,
            userAvatar: userInfo.userAvatar             
          })
          wx.setStorageSync('storeList', res.data)
        }
      })
    }
    }
    
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})