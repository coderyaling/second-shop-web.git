// pages/my/my.js
const util = require('../../request/index')

Page({

  /**
   * 组件的初始数据
   */
  data: {
    orderBarList: [{
        statusId: 1,
        url: '/pages/orderList/orderList',
        iconClass: 'daizhifu',
        name: '待支付'
      },
      {
        statusId: 2,
        url: '/pages/orderList/orderList',
        iconClass: 'daifahuo',
        name: '待发货'
      },
      {
        statusId: 3,
        url: '/pages/orderList/orderList',
        iconClass: 'yifahuo',
        name: '已发货'
      },
      {
        statusId: 4,
        url: '/pages/orderList/orderList',
        iconClass: 'yiwancheng',
        name: '已完成'
      }
    ],
    infoBarList: [{
        url: '/pages/store/store',
        iconClass: 'dianpu',
        name: '我的店铺'

      },
      {
        url: '/pages/transferor/transferor',
        iconClass: 'zhuanrang',
        name: '我转让的'

      },
      {
        url: '/pages/message/message',
        iconClass: 'xiaoxi',
        name: '我的消息'

      },
      {
        url: '/pages/schoolService/schoolService',
        iconClass: 'fuwu',
        name: '校园服务'

      },
      {
        url: '/pages/feedback/feedback',
        iconClass: 'fankui',
        name: '意见反馈'

      },
      {
        url: '/pages/addressList/addressList',
        iconClass: 'dizhi',
        name: '地址管理'

      },
      {
        url: '/pages/aboutUs/aboutUs',
        iconClass: 'guanyu',
        name: '关于我们'

      },
      {
        url: '/pages/bindPhone/bindPhone',
        iconClass: 'bangding',
        name: '绑定手机'

      }
    ],
    myInfo: null,
    orderBarDataList: [],
    userInfo: null
  },
  onShow: function () {
    if (typeof this.getTabBar === 'function' && this.getTabBar()) {
      this.getTabBar().setData({
        selected: 2
      })
    }
    var userInfo = wx.getStorageSync('userInfo')
    if (userInfo) {
      this.setData({
        userInfo
      })
    }
    this.getMyInfoFun();

  },
  onLoad: function (e) {
  },
  async getMyInfoFun() {
    if (this.data.userInfo != null) {
      console.log("打印");
      var myInfo = wx.getStorageSync('myInfo');
      var orderBarDataList = wx.getStorageSync('orderBarDataList');
      if (myInfo && orderBarDataList) {
        this.setData({
          myInfo,
          orderBarDataList
        })
      } else {
        // 获取我的页面的数据
        util.request_get('/web/user/userInfo', {
          userId: '22080000'
        }).then((res) => {
          this.setData({
            myInfo: res.data
          })
          // 将数据存入列表中 实现循环
          const orderInfo = this.data.myInfo
          var orders = [orderInfo.waitPayNum, orderInfo.waitDeliveryNum, orderInfo.waitReceiveNum]
          this.setData({
            orderBarDataList: orders
          })
          wx.setStorageSync('myInfo', res.data)
          wx.setStorageSync('orderBarDataList', orders)
        })

      }
    }
  },
  Removedata() {

    wx.clearStorageSync(); //清除缓存
    wx.showToast({
      title: '退出登录成功',
      icon: 'none',
      duration: 2000,
      success: function () {
        setTimeout(function () {
          //跳转到首页，强制重启
          wx.reLaunch({
            url: '/pages/index/index',
          })
        }, 1000);
      }
    })

  },
  Updateversion() {
    wx.showModal({
      title: '提示',
      content: '此前就是最新版本（2.07）',
      success: function (res) {
        if (res.confirm) { //这里是点击了确定以后
          console.log('用户点击确定')
        } else { //这里是点击了取消以后
          console.log('用户点击取消')
        }
      }
    })
  },
  Logout() {
    wx.showModal({
      title: '提示',
      content: '退出当前登入吗？',
      success: function (res) {
        if (res.confirm) { // 这里是点击了确定以后
          // 首先调用接口退出程序登录 

          // 然后删除缓存数据
          wx.removeStorageSync('userInfo')
          setTimeout(function () {
            //跳转到首页，强制重启
            wx.reLaunch({
              url: '/pages/my/my',
            })
          }, 100);
        } else { // 这里是点击了取消以后
          console.log('用户点击取消')
        }
      }
    })
  }
})