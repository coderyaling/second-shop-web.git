// pages/getCode/getCode.js
const util = require('../../request/index')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    veCode: new Array(),
    time: 60,
    userPhone: ''
  },
  inputValue(e) {
    let value = e.detail.value;
    let arr = [...value];
    this.setData({
      veCode: arr
    })
  },
  againTime() {
    let time = this.data.time;
    clearInterval(timing);
    let timing = setInterval(() => {
      if (time <= 0) {
        clearInterval(timing)
      } else {
        time--;
        this.setData({
          time: time
        })
      }
    }, 1000)
    // 发送接口 注册接口 发送验证码接口 
    console.log(this.data.userPhone);
    util.request_post('/web/phone/code', {
      userPhone: this.data.userPhone
    }).then((res) => {
      if (res.code == 200) {
        wx.showToast({
          title: '发送验证码成功',
        })
      }
    })
  },
  againTimeBtn() {
    this.setData({
      time: 60
    });
    this.againTime()
  },

  // 注册按钮
  registerBtn() {
    // 发送注册接口 调用注册接口
    // 需要先把weCode 数组连接为字符串
    //将数组对象转为字符串数组
    var code = JSON.stringify(this.data.veCode)
    util.request_post('/web/register', {
      userPhone: this.data.userPhone,
      code: code
    }).then((res) => {
      if (res.code == 200) {
        wx.showToast({
          title: '注册成功！',
        })
        // 调用获取信息接口
        this.getUserInfo()
        // 跳转到主页页面
        wx.navigateTo({
          url: '/pages/index/index',
        })
      }
    })
  },
  getUserInfo() {
    var userInfo = wx.getStorageSync('userInfo')
    if (userInfo) {
      this.setData({
        userInfo
      })
    } else {
      util.request_get('/web/user/personal-info', {
        userId: this.data.userPhone
      }).then((res) => {
        if (res.code == 200) {
          // 下次报错的话  先看看你响应结果有没有成功 
          this.setData({
            userInfo: res.userInfo
          })
          // 存到全局变量
          wx.setStorageSync('userInfo', res.userInfo)
          app.globalData.userInfo = res.userInfo
        }
      })
    }

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      userPhone: options.userPhone
    })
    this.againTime()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})