const util = require('../../request/index')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    allOrderList: [],
    tabs: [{
        id: 0,
        name: "全部订单",
        isActive: true
      },
      {
        id: 1,
        name: "待支付",
        isActive: false
      },
      {
        id: 2,
        name: "待发货",
        isActive: false
      },
      {
        id: 3,
        name: "已发货",
        isActive: false
      },
      {
        id: 4,
        name: "已完成",
        isActive: false
      }
    ]
  },
  handleItemChange(e) {
    const {
      index
    } = e.detail;
    let {
      tabs
    } = this.data;
    tabs.forEach((v, i) => i === index ? v.isActive = true : v.isActive = false);
    this.setData({
      tabs
    });
  },
  onShow: function () {
    this.getAllOrderList();
  },
  async getAllOrderList() {
    var userInfo = wx.getStorageSync('userInfo')
    if (userInfo) {
      var orderList = wx.getStorageSync('orderList')
    if (orderList) {
      this.setData({
        allOrderList: orderList
      }) 
    } else {
      util.request_get('/web/order/buyer-list', {
        userId: userInfo.userId
      }).then((res) => {
        if (res.code == 200) {
          // 下次报错的话  先看看你响应结果有没有成功
          this.setData({
            allOrderList: res.data
          })
          wx.setStorageSync('orderList', res.data)
        }
      })

    }
    }
  },
  cancleOrder() {
    util.request_post('/web/order/cancel-order', {
      orderId: this.data.orderId
    }).then((res) => {
      if (res.code == 200) {
        wx.showToast({
          title: '取消成功！',
        })
        wx.removeStorageSync('orederList')
      } else {
        wx.showToast({
          title: '取消失败!',
          icon :'none'
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 这个是接受传过来的参数
    // 改变当前状态  这个options里的参数全是字符串 记住
    var 
      indexActive = options.statusId;
    if (indexActive == undefined) {
      indexActive = 0
    }
    let {
      tabs
    } = this.data;
    // 这里不能用 ===  
    tabs.forEach((v, i) => {
      i == indexActive ? v.isActive = true : v.isActive = false;
    });
    this.setData({
      tabs
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})