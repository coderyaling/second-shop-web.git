// index.js
// 引入封装好的请求数据的js文件
const util = require('../../request/index')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabs: [{
        id: 0,
        name: "商品详情",
        isActive: true
      },
      {
        id: 1,
        name: "发布信息",
        isActive: false
      },
      {
        id: 3,
        name: "评论",
        isActive: false
      }
    ],
    oldLevel: ['一', '二', '三', '四', '五', '六', '七', '八', '九'],
    goodsId: null,
    goodsInfo: null,
    userInfo: null,
    parentCommentId: 0, 
    soldOutNum: 0,
    isLike: false,
    isCollect: false,
    commentList: [],
    commentLength: 0,
    isReply: false,
    isComment: false,
    commentForm: {
      commentId: null,
      userId: "",
      commentContent: "",
      goodsId: 0,
      parentCommentId: 0,
      goodsComment: null,
      commmentStatus: null,
      createTime: "",
      commentLikeNum: null,
    },
    commentContent: ''
  },
  handleItemChange(e) {
    const {
      index
    } = e.detail;
    let {
      tabs
    } = this.data;
    tabs.forEach((v, i) => i === index ? v.isActive = true : v.isActive = false);
    this.setData({
      tabs
    })
  },

  /**
   * 生命周期函数--监听页面加载
   * 上一个页面传递过来的数据都会在options里
   */
  onLoad: function (options) {
    var app = getApp();
    let {
      Modelmes
    } = app.globalData;
    this.setData({
      Modelmes
    });
    // 设置物品ID参数
    this.setData({
      goodsId: options.goodsId
    })
  },
  async getGoodsInfo() {
    util.request_get('/web/index/goods/info', {
      goodsId: this.data.goodsId,
      userId: '22080000'
    }).then((res) => {
      if (res.code === 200) {
        // 下次报错的话  先看看你响应结果有没有成功
        console.log(res);
        this.setData({
          goodsInfo: res.goodInfo,
          userInfo: res.userBelong,
          soldOutNum: res.userSoldOutNum,
          isLike: res.isLike,
          isCollect: res.isCollect
        })
      }
    })
  },
  async collectGoods() {
    // 调用收藏的接口  更改是否收藏
    const that = this.data;
    if (that.isCollect) {
      // 调用取消收藏
      util.request_post('/web/user/cancel-collect', {
        userId: '22080000',
        goodsId: this.data.goodsId
      }).then((res) => {
        if (res.code === 200) {
          wx.showToast({
            title: '取消成功',
            icon: 'success',
            duration: 1000 //持续的时间
          })
        } else {
          wx.showToast({
            title: '取消失败',
            icon: 'warn',
            duration: 1000

          })
        }
      })
    } else {
      // 收藏当前物品
      util.request_post('/web/user/append-collect', {
        userId: '22080000',
        goodsId: this.data.goodsId
      }).then((res) => {
        if (res.code === 200) {
          wx.showToast({
            title: '收藏成功',
            icon: 'success',
            duration: 1000 //持续的时间
          })
        } else {
          wx.showToast({
            title: '收藏失败',
            icon: 'warn',
            duration: 1000

          })
        }
      })
    }
    this.setData({
      isCollect: !this.data.isCollect
    })
  },
  async likeGoods() {
    // 调用点赞的接口  更改是否点赞
    const that = this.data;
    if (that.isLike) {
      // 调用取消点赞
      util.request_post('/web/user/cancel-like', {
        userId: '22080000',
        goodsId: this.data.goodsId
      }).then((res) => {
        if (res.code === 200) {
          wx.showToast({
            title: '取消成功',
            icon: 'success',
            duration: 1000 //持续的时间
          })
        } else {
          wx.showToast({
            title: '取消失败',
            icon: 'warn',
            duration: 1000

          })
        }
      })
    } else {
      // 点赞当前物品
      util.request_post('/web/user/append-like', {
        userId: '22080000',
        goodsId: this.data.goodsId
      }).then((res) => {
        if (res.code === 200) {
          wx.showToast({
            title: '点赞成功',
            icon: 'success',
            duration: 1000 //持续的时间
          })
        } else {
          wx.showToast({
            title: '点赞失败',
            icon: 'warn',
            duration: 1000
          })
        }
      })
    }
    this.setData({
      isLike: !this.data.isLike
    })
  },
  async getCommentList() {
    util.request_get('/web/comment/list', {
      goodsId: this.data.goodsId,
      userId: '22080000'
    }).then((res) => {
      if (res.code === 200) {
        // 下次报错的话  先看看你响应结果有没有成功
        console.log(res); // 这里不是打印了吗 
        this.setData({
          commentList: res.comments
        })
        var replyLength = 0;
        this.data.commentList.map(item => {
          replyLength += item.replyComments.length
        })
        this.setData({
          commentLength: replyLength + this.data.commentList.length
        })
      }
    })
  },
  commentContextInput(e) {
    this.setData({
      commentContent: e.detail.value
    })
  },
  // 发表评论
  pulishBtn() {
    var userInfo = wx.getStorageSync('userInfo')
    if (userInfo) {
      // 设置基本数据
      this.setData({
        ['commentForm.userId']: userInfo.userId,
        ['commentForm.commentContent']: this.data.commentContent,
        ['commentForm.goodsId']: this.data.goodsId,
        ['commentForm.parentCommentId']: this.data.parentCommentId,
        ['commentForm.goodsComment']: userInfo.userId == this.data.goodsInfo.goodsUserId ? 2 : 1,
      })
      util.request_json_post('/web/comment/insert', this.data.commentForm).then((res) => {
        if (res.code == 200) {
          wx.showToast({
            title: '评论成功',
          })
          // 设置数据
          this.getCommentList()
        } else {
          wx.showToast({
            title: '评论失败',
          })
        }
      })
    } else {
      wx.showToast({
        title: '请先登录！',
        icon: 'none'
      })

    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getGoodsInfo();
    this.getCommentList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})