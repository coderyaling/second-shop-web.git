// pages/register/register.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isChecked: false,
    userPhone: ''
  },
  bindExchangePassShow() {
    this.setData({
      isShowPass: !this.data.isShowPass
    })
  },

  userPhoneInput(e) {
    this.setData({
      userPhone: e.detail.value
    })
  },

  toGetCodePage() {
    // 需要先判断是否勾选
    if (this.data.userPhone.length != 11) {
      wx.showToast({
        title: '请输入正确的手机号码！',
        icon:'none'
      })
    } else {
      if (this.data.isChecked) {
        wx.navigateTo({
          url: '/pages/getCode/getCode?userPhone='+this.data.userPhone,
        })
      } else {
        wx.showToast({
          title: '请勾选隐私政策',
        })
      }
    }
  },

  bindChangeChecked() {
    this.setData({
      isChecked: !this.data.isChecked
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})