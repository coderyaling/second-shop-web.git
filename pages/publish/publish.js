//引入事先写好的日期设置util.js文件
var datePicker = require('../../utils/dateSetting.js')
const util = require('../../request/index')
//导入验证js
import WxValidate from "../../utils/WxValidate";
//设定当前的时间，将其设定为常量
const date = new Date();
const year = date.getFullYear();
const month = date.getMonth() + 1;

Page({

  /**
   * 组件的初始数据
   */
  data: {
    imagesList: [],
    goodsTypeList: [],
    imageMaxCount: 5,
    //设置当前完成步数
    // input默认是1    
    region: ['江西省', '南昌市', '青山湖'], // 初始值
    goodsDetailAddress: "",
    listData: [10, 9, 8, 7, 6, 5, 4, 3, 2, 1],
    goodsStock: 1,
    minusStatus: 'disabled',
    currentStep: 0,
    stepsList: ["步骤一", "步骤二", "步骤三"],
    stepNameList: ["物品信息", "卖家信息", "发布成功"],
    goodsNameCount: 0,
    goodsDescCount: 0,
    progress: 67,
    goodsLocalAddress: '',
    percent: 33.33,
    time: '',
    multiArray: [],
    multiIndex: [0, 0, 0, 0, 0],
    choose_year: "",
    goodsDealType: '3',
    items: [{
        value: '1',
        name: '面交',
      },
      {
        value: '2',
        name: '邮寄'
      },
      {
        value: '3',
        name: '自提'
      },
    ],
    goodsForm: {
      "goodsId": null,
      "goodsName": "",
      "goodsDes": "",
      "goodsOriginalPrice": null,
      "goodsSecondPrice": null,
      "goodsStock": 0,
      "goodsOld": null,
      "goodsTitle": "",
      "goodsIndate": "",
      "goodsStatus": 1,
      "goodsPhone": "",
      "goodsQq": "",
      "goodsAddress": "",
      "goodsUserId": "",
      "goodsDealType": "",
      "createTime": "",
      "updateTime": "",
      "goodsViews": 0,
      "goodsImage": [],
      "goodsType": null,
      "goodUsername": null,
      "userAvatar": null
    },
    goodsInfo: {}
  },
  onShow: function () {
    if (typeof this.getTabBar === 'function' && this.getTabBar()) {
      this.getTabBar().setData({
        selected: 1
      })
    }
    this.getTypeList();
  },

  // 点击
  checkedType(e) {
    let index = e.currentTarget.dataset.index
    var typeList = this.data.goodsTypeList
    // 取反
    typeList[index].checked = !typeList[index].checked
    this.setData({
      goodsTypeList: typeList
    })
    console.log(e);
  },

  prePage() {
    this.setData({
      currentStep: this.data.currentStep - 1
    })
  },

  // 绑定数据
  goodsNameInput(e) {
    var goodsName = e.detail.value; //输入的内容
    var value = e.detail.value.length; //输入内容的长度
    var lastArea = 15 - value; //剩余字数
    var that = this;
    that.setData({
      ['goodsForm.goodsName']: goodsName,
      goodsNameCount: lastArea
    })
  },

  // 绑定数据
  goodsDescInput(e) {
    var goodsDesc = e.detail.value; //输入的内容
    var value = e.detail.value.length; //输入内容的长度
    var lastArea = 100 - value; //剩余字数
    var that = this;
    that.setData({
      ['goodsForm.goodsDes']: goodsDesc,
      goodsDescCount: lastArea
    })
  },

  choosePicture() {
    let that = this
    if (this.data.imagesList.length > 5) {
      // 不能添加
      return;
    }
    var tmpList = this.data.imagesList
    wx.chooseImage({
      count: that.data.imageMaxCount - tmpList.length, // 默认3
      sizeType: ["original", "compressed"], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ["album", "camera"], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        var uploadImages = res.tempFilePaths;
        uploadImages.forEach(item => {
          tmpList.push(item)
        });
        that.setData({
          imagesList: tmpList
        })
      }
    })
  },
  detelePhoto(e) {
    var list = this.data.imagesList;
    list.splice(e.currentTarget.dataset.index, 1)
    this.setData({
      imagesList: list
    })
  },
  async getTypeList() {
    // 获取到分类列表就添加一个checked属性
    util.request_get('/web/goods-type/list').then((res) => {
      if (res.code === 200) {
        // 添加参数
        var goodsTypes = res.goodsTypes
        let _goodsTypes = []
        // goodsTypes 需要更改的字段
        // map是一个遍历[]列表的方法 然后参数一：item是遍历的每个对象
        goodsTypes.map((item) => {
          // push还记得吧  Object.assign({}, item, {checked: false}) 是vue的js 一样的  一个对象，item就是目标对象 后面的{}就是需要插入的对象  然后点击按钮取反 就ok
          _goodsTypes.push(Object.assign({}, item, {
            checked: false
          }))
        })
        console.log(_goodsTypes);
        this.setData({
          goodsTypeList: _goodsTypes
        })
      }

    })
  },
  //设置单选value
  radioChange: function (e) {
    var that = this;
    that.setData({
      goodsDealType: e.detail.value
    })
  },
  // 下一步
  nextStep() {
    // 点击下一步  将图片和分类全部赋值给表单
    // 照片 将imageList 直接data  
    // 将分类的checked为true 拼接成1,2,3,4
    // 使用map来遍历
    var goodsType = ""
    let goodsTypeList = this.data.goodsTypeList
    console.log(goodsTypeList,"打印");
    goodsTypeList.map(item => {
      // 获取当前对象 判断checked是否为true  如果为true 则拼接
      if (item.checked) {
        // 如果为true 就拼接 但是这里有一个bug需要解决  就是goodsType开始是为空 然后加了一个、使用需要先判断是否为空
        console.log(item);
        goodsType = goodsType + (goodsType == "" ? "" : "、") + item.goodsTypeId
      }
    })
    this.setData({
      currentStep: this.data.currentStep + 1,
      ['goodsForm.goodsImage']: this.data.imagesList,
      ['goodsForm.goodsType']: goodsType,
      ['goodsForm.goodsStock']: this.data.goodsStock,
    })
  },

  bindMinus: function () {
    var goodsStock = this.data.goodsStock;
    // 如果大于1时，才可以减   
    if (goodsStock > 1) {
      goodsStock--;
    }
    // 只有大于一件的时候，才能normal状态，否则disable状态    
    var minusStatus = goodsStock <= 1 ? 'disabled' : 'normal';
    // 将数值与状态写回    
    this.setData({
      goodsStock: goodsStock,
      minusStatus: minusStatus
    });
  },

  // 绑定原价数据
  originPriceInput(e) {
    this.setData({
      ['goodsForm.goodsOriginalPrice']: e.detail.value
    })
  },

  goodsOldInput(e) {
    let value = this.validateNumber(e.detail.value)
    console.log(value);
    if (value <= 10) {
      console.log(value);
      this.setData({
        ['goodsForm.goodsOld']: value
      })
    }
  },
  validateNumber(val) {
    return val.replace(/\D/g, '')
  },

  // 绑定原价数据
  secondPriceInput(e) {
    this.setData({
      ['goodsForm.goodsSecondPrice']: e.detail.value
    })
  },

  bindRegionChange: function (e) { // picker值发生改变都会触发该方法
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      region: e.detail.value
    })
  },
  bindPlus: function () {
    var goodsStock = this.data.goodsStock;
    // 不作过多考虑自增1   
    goodsStock++;
    // 只有大于一件的时候，才能normal状态，否则disable状态     
    var minusStatus = goodsStock < 1 ? 'disabled' : 'normal';
    // 将数值与状态写回     
    this.setData({
      goodsStock: goodsStock,
      minusStatus: minusStatus
    });
  },
  /* 输入框事件 */
  bindManual: function (e) {
    var goodsStock = e.detail.value;
    // 将数值与状态写回     
    this.setData({
      goodsStock: goodsStock
    });
  },
  goodsTitleInput(e) {
    this.setData({
      ['goodsForm.goodsTitle']: e.detail.value
    })
  },

  goodsPhoneInput(e) {
    this.setData({
      ['goodsForm.goodsPhone']: e.detail.value
    })
  },
  goodsQQInput(e) {
    this.setData({
      ['goodsForm.goodsQq']: e.detail.value
    })
  },

  goodsDetailAddressInpput(e) {
    this.setData({
      goodsDetailAddress: e.detail.value
    })
  },

  goodsLocalAddressInput() {
    this.setData({
      goodsLocalAddress: e.detail.value
    })
  },

  // 提交物品
  submitGoods: function () {
    // 需要拼接一下地址 然后存到 goodsAddress里面 我后端根据格式解析
    var goodsAddress = this.data.region[0] + this.data.region[1] + this.data.region[2] + this.data.goodsDetailAddress + "-" + this.data.goodsLocalAddress
    this.setData({
      ['goodsForm.goodsIndate']: this.data.time,
      ['goodsForm.goodsAddress']: goodsAddress,
      ['goodsForm.goodsDealType']: this.data.goodsDealType,
      ['goodsForm.goodsUserId']: '22080000'
    })

    console.log(this.data.goodsForm);

    // 然后校验表单 和 调用接口
    // 先创建一个校验器 
    if (!this.WxValidate.checkForm(this.data.goodsForm)) {
      const error = this.WxValidate.errorList[0]
      this.showModal(error)
      return false
    }
    console.log("提交数据");
    console.log(this.data.goodsForm);
    // 调用post 请求
    util.request_json_post("/web/goods/addition-goods", this.data.goodsForm).then(resp => {
      if (resp.code == 200) {
        wx.showToast({
          title: '发布成功',
          icon: 'success',
          duration: 1000 //持续的时间
        })
        this.setData({
          goodsInfo: resp.goodsInfo,
          currentStep: this.data.currentStep + 1
        })
      } else {
        wx.showToast({
          title: '发布失败',
          icon: 'none',
          duration: 1000 //持续的时间
        })
      }
    })
  },

  initValidate() {
    let rules = {
      goodsName: {
        required: true,
        maxlength: 15
      },
      goodsDes: {
        required: true,
        maxlength: 100
      },
      goodsImage: {
        required: true,
      },
      goodsType: {
        required: true
      },
      goodsOriginalPrice: {
        required: true
      },
      goodsSecondPrice: {
        required: true
      },
      goodsTitle: {
        required: true,
        maxlength: 50
      },
      goodsIndate: {
        required: true
      },
      goodsAddress: {
        required: true
      },
      goodsOld: {
        required: true,
      }

    }

    let message = {
      goodsName: {
        required: '请输入物品名称',
        maxlength: '物品名称不能超过15个字'
      },
      goodsType: {
        required: '请选择分类'
      },
      goodsDes: {
        required: "请输入物品描述",
        maxlength: '物品描述不能超过100个字'
      },
      goodsImage: {
        required: "请选择物品图片"
      },
      goodsType: {
        required: "请选择物品分类"
      },
      goodsOriginalPrice: {
        required: "请填写原价"
      },
      goodsSecondPrice: {
        required: "请填写物品二手价"
      },
      goodsTitle: {
        required: "请输入物品标题",
        maxlength: "长度只能50字以内"
      },
      goodsIndate: {
        required: "请选择有效期"
      },
      goodsAddress: {
        required: "请填写地址"
      },
      goodsOld: {
        required: "请输入成色"
      }
    }
    //实例化当前的验证规则和提示消息
    this.WxValidate = new WxValidate(rules, message);
  },


  //报错 
  showModal(error) {
    wx.showModal({
      content: error.msg,
      showCancel: false,
    })
  },


  // 绑定数据 

  //Page原始的加载函数，设定multiArray，其中datePicker中的函数，会在下面的js中呈现。
  onLoad: function () {
    this.setData({
      multiArray: [
        [year + "年", year + 1 + "年", year + 2 + "年"],
        datePicker.determineMonth(),
        datePicker.determineDay(year, month),
        datePicker.determineHour(),
        datePicker.determineMinute()
      ],
    })
    // 初始化校验器
    this.initValidate();
  },
  //最后呈现时间的函数。
  bindMultiPickerChange: function (e) {
    console.log(this.data.multiArray);
    var dateStr = this.data.multiArray[0][this.data.multiIndex[0]].split('年')[0] + "-"+
      this.data.multiArray[1][this.data.multiIndex[1]].split('月')[0] + "-"+
      this.data.multiArray[2][this.data.multiIndex[2]].split('日')[0] + " " +
      this.data.multiArray[3][this.data.multiIndex[3]].split('时')[0] + ":" +
      this.data.multiArray[4][this.data.multiIndex[4]].split('分')[0] + ":00";
    this.setData({
      time: dateStr
    })
  },
  //当时间选择器呈现并进行滚动选择时间时调用该函数。
  bindMultiPickerColumnChange: function (e) {
    //e.detail.column记录哪一行发生改变，e.detail.value记录改变的值（相当于multiIndex）
    switch (e.detail.column) {
      //这里case的值有0/1/2/3/4,但除了需要记录年和月来确定具体的天数外，其他的都可以暂不在switch中处理。
      case 0:
        //记录改变的年的值
        let year = this.data.multiArray[0][e.detail.value];
        this.setData({
          choose_year: year.substring(0, year.length - 1)
        })
        break;
      case 1:
        //根据选择的年与月，确定天数，并改变multiArray中天的具体值
        let month = this.data.multiArray[1][e.detail.value];
        let dayDates = datePicker.determineDay(this.data.choose_year, month.substring(0, month.length - 1));
        //这里需要额外注意，改变page中设定的data，且只要改变data中某一个值，可以采用下面这种方法
        this.setData({
          ['multiArray[2]']: dayDates
        })
        break;
    }
    //同上，上面改变的是二维数组中的某一个一维数组，这个是改变一个一维数组中某一个值，可供参考。
    this.setData({
      ["multiIndex[" + e.detail.column + "]"]: e.detail.value
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})