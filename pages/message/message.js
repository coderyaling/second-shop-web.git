// pagesstst.js
const util = require('../../request/index')
const App = getApp()
var dateTimePicker = require('../../utils/dateTimePicker.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {

    messageList: [],
    // 设置开始的位置
    startX: 0,
    startY: 0

  },
  onLoad: function () {
    this.initDate()
  },

  // 初始化时间
  initDate() {
    // 获取完整的年月日 时分秒，以及默认显示的数组
    var obj = dateTimePicker.dateTimePicker(this.data.startYear, this.data.endYear);
    var obj1 = dateTimePicker.dateTimePicker(this.data.startYear, this.data.endYear);
    // 精确到分的处理，将数组的秒去掉
    // var lastArray = obj1.dateTimeArray.pop();
    // var lastTime = obj1.dateTime.pop();

    this.setData({
      dateTime: obj.dateTime,
      dateTimeArray: obj.dateTimeArray,
      dateTimeArray1: obj1.dateTimeArray,
      dateTime1: obj1.dateTime
    });
  },

  //手指触摸动作开始 记录起点X坐标
  touchstart: function (e) {
    //开始触摸时 重置所有删除
    let data = App.touch._touchstart(e, this.data.messageList) //将修改过的list setData
    this.setData({
      messageList: data
    })
  },
  //滑动事件处理
  touchmove: function (e) {
    let data = App.touch._touchmove(e, this.data.messageList)//将修改过的list setData
    this.setData({
      messageList: data
    })
  },
  //删除事件
  del: function (e) {
    this.data.messageList.splice(e.currentTarget.dataset.index, 1)
    this.setData({
      messageList: this.data.messageList
    })
  },
  //跳转
  goDetail() {
    console.log('点击元素跳转')
  },

  changeDate(e) {
    this.setData({
      date: e.detail.value
    });
  },
  changeTime(e) {
    this.setData({
      time: e.detail.value
    });
  },
  changeDateTime(e) {
    this.setData({
      dateTime: e.detail.value
    });
  },
  changeDateTime1(e) {
    this.setData({
      dateTime1: e.detail.value
    });
  },
  changeDateTimeColumn(e) {
    var arr = this.data.dateTime,
      dateArr = this.data.dateTimeArray;

    arr[e.detail.column] = e.detail.value;
    dateArr[2] = dateTimePicker.getMonthDay(dateArr[0][arr[0]], dateArr[1][arr[1]]);

    this.setData({
      dateTimeArray: dateArr,
      dateTime: arr
    });
  },
  changeDateTimeColumn1(e) {
    var arr = this.data.dateTime1,
      dateArr = this.data.dateTimeArray1;

    arr[e.detail.column] = e.detail.value;
    dateArr[2] = dateTimePicker.getMonthDay(dateArr[0][arr[0]], dateArr[1][arr[1]]);

    this.setData({
      dateTimeArray1: dateArr,
      dateTime1: arr
    });
  },

  async getMessageList() {
    var messageList = wx.getStorageSync('messageList')
    if (messageList) {
      this.setData({
        messageList: messageList
      })
    } else {
      util.request_get('/web/notices/user/list', {
        userId: '22080000'
      }).then((res) => {
        if (res.code == 200) {
          // 下次报错的话  先看看你响应结果有没有成功 
          this.setData({
            messageList: res.data
          })
          wx.setStorageSync('messageList', res.messageList)
        }
      })
    }
  },
  onShow: function () {
    this.getMessageList();
  }
})