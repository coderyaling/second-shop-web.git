// pages/feedback/feedback.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imageList: [],
    imageMaxCount: 3,
    feedbackDesc: '',
    feedbackPhone: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },
  feedbackDescText(e) {
    this.setData({
      feedbackDesc: e.detail.value
    })
  },
  feedbackPhoneInput(e) {
    this.setData({
      feedbackPhone: e.detail.value
    })
  },
  submitFeedback() {
    console.log(this.data.feedbackDesc);
    console.log(this.data.feedbackPhone);
    console.log(this.data.imageList);
  },

  choosePicture() {
    let that = this
    if (this.data.imageList.length > 3) {
      // 不能添加
      return;
    }
    var tmpList = this.data.imageList
    wx.chooseImage({
      count: that.data.imageMaxCount - tmpList.length, // 默认3
      sizeType: ["original", "compressed"], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ["album", "camera"], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        var uploadImages = res.tempFilePaths;
        uploadImages.forEach(item => {
          tmpList.push(item)
        });
        that.setData({
          imageList: tmpList
        })
      }
    })
  },
  detelePhoto(e) {
    var list = this.data.imageList;
    list.splice(e.currentTarget.dataset.index, 1)
    this.setData({
      imageList: list
    })
  },


  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})