// pages/wait_payment/wait_payment.js
const util = require('../../request/index')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderInfo: {},
    orderId: '',
    countdown: ""
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    this.setData({
      orderId: options.orderId
    })
  },
  countDown() {
    var that = this
    // 开始时间肯定要是订单创建的时间呀  我先给你关了 不然写不了
    var starttime = '2022/10/16 16:35:19'
    // var starttime = that.data.orderInfo.createTime
    console.log(starttime);
    var start = new Date(starttime.replace(/-/g, "/")).getTime()
    var endTime = start + 15 * 60000
    var date = new Date(); //现在时间
    var now = date.getTime(); //现在时间戳
    var allTime = endTime - now
    var m, s;
    console.log(allTime);
    if (allTime > 0) {
      m = Math.floor(allTime / 1000 / 60 % 60);
      s = Math.floor(allTime / 1000 % 60);
      if (m == 0) {
        that.setData({
          countdown: s + "s",
        })
      } else {
        that.setData({
          countdown: m + ":" + s,
        })
      }
      setTimeout(that.countDown, 1000);
    } else {
      console.log('已截止')
      that.setData({
        countdown: "订单超时"
      })

      // 超时就要调用订单删除接口
      util.request_post('/web/order/cancel-order', {
        orderId: this.data.orderInfo.orderId
      }).then((res) => {
        if (res.code ===200) {
          // 提示用户
          wx.showToast({
            title: '订单超时',
          })
        }
      })
      // 然后需要清除orderList的缓存  然后清除缓存
      wx.removeStorageSync('orderList')

    }
  },
  async getWaitPay() {
    util.request_get('/web/order/query-order', {
      orderId: this.data.orderId
    }).then((res) => {
      console.log(res);
      if (res.code === 200) {
        this.setData({
          orderInfo: res.orderInfo
        })
        this.countDown()
      }

    })
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getWaitPay();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})