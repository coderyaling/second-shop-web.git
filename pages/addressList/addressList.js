const util = require('../../request/index')
Page({
  data: {
    aa: '',
    addressList: []
  },

  onLoad: function (options) {
    var arr = wx.getStorageSync('addressList') || [];
    // 更新数据  
    this.setData({
      addressList: arr,
    });
  },

  /**
   * 生命周期函数--监听页面显示
   */
  async getAddressList() {
    util.request_get('/web/address/user/list', {
      userId: '22080000'
    }).then((res) => {
      if (res.code === 200) {
        // 下次报错的话  先看看你响应结果有没有成功
        console.log(res); // 这里不是打印了吗 
        this.setData({
          addressList: res.data
        })
      }
    })
  },

  onPullDownRefresh: function () {
    this.onRefresh();
  },
  onRefresh: function () {
    //导航条加载动画
    wx.showNavigationBarLoading();
    setTimeout(function () {
      wx.hideNavigationBarLoading();
      //停止下拉刷新
      wx.stopPullDownRefresh();
    }, 2000);

  },

  onShow: function () {
    this.onLoad();
    this.getAddressList();
  },

  onUnload: function () {
    var arr = this.data;
    wx.setStorageSync('addressList', arr);

  },

  addAddress: function () {
    wx.navigateTo({
      url: '/pages/addLocation/addLocation'
    });
  },
  // 修改默认的选中 同时更新发送切换请求
  radioChange: function (e) {
    let that = this
    wx.showModal({
      title: '提示',
      content: '是否设置为默认',
      success(res) {
        if (res.confirm) {
          console.log(e);
          if (1 == e.currentTarget.dataset.addressstatus) return;
          let addressList = that.data.addressList
          for (var i = 0; i < addressList.length; i++) {
            if (e.currentTarget.dataset.addressstatus == 2) {
              addressList[i].addressStatus = 2
            }
            if (e.currentTarget.dataset.index == i) {
              addressList[i].addressStatus = e.currentTarget.dataset.addressstatus == 1 ? 2 : 1;
            }
          }
          // 那这个比较简单  就判断状态是1 就改为2 反之
          // 先修改小程序本地的数据  再发送请求  知道不
          // 修改渲染数据
          that.setData({
            addressList
          })
          // 发送修改默认地址请求
          util.request_get('/web/address/update/default', {
            userId: '22080000',
            addressId: e.currentTarget.dataset.addressid
          }).then((res) => {
            if (res.code !== 200) {
              wx.showToast({
                title: '修改失败',
                icon: 'none',
                duration: 1000
              })
            }
          })

        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })



  },

  updateAddress: function (e) {
    wx.navigateTo({
      url: '/pages/addLocation/addLocation?addressId='+ e.currentTarget.dataset.addressid,
    })
  },
  /* 删除item */

  delAddress: function (e) {
    var id = e.currentTarget.dataset.id //数组下标
    let that = this;
    wx.showModal({
      title: '提示',
      content: '是否删除',
      success(res) {
        if (res.confirm) {
          that.data.addressList.splice(id, 1);
          // 更新data数据对象  
          if (that.data.addressList.length > 0) {
            this.setData({
              addressList: this.data.addressList
            })
            wx.setStorageSync('addressList', this.data.addressList);
          } else {
            that.setData({
              addressList: that.data.addressList
            })
            wx.setStorageSync('addressList', []);
          }
          util.request_post('/web/address/delete', {
            addressId: e.currentTarget.dataset.addressid
          }).then((res) => {
            if (res.code !== 200) {
              wx.showToast({
                title: '删除失败',
                icon: 'none',
                duration: 1000
              })
            }
          })
        }
      }
    })
  }
})