// pages/login.js
const util = require('../../request/index')
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
    isShowPass: true,
    loginForm: {
      userId: '',
      password: '',
    },
    isChecked: false
  },

  // 取法
  bindExchangePassShow() {
    this.setData({
      isShowPass: !this.data.isShowPass
    })
  },

  bindChangeChecked() {
    this.setData({
      isChecked: !this.data.isChecked
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },
  bindAccountInput(e) {
    this.setData({
      ['loginForm.userId']: e.detail.value
    })
  },
  bindPasswordInput(e) {
    this.setData({
      ['loginForm.password']: e.detail.value
    })
  },
  loginBtn() {
    if (this.data.loginForm.userId.length >= 8) {
      if (/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,16}$/.test(this.data.loginForm.password)) {
        if (this.data.isChecked) {
          util.request_json_post('/web/login', this.data.loginForm).then((res) => {
            console.log(res);
            if (res.code == 200) {
              wx.showToast({
                title: '登录成功!',
              })
              this.getUserInfo();
              
            } else {
              wx.showToast({
                title: res.msg,
                icon: 'none'
              })
            }
          })
        } else {
          wx.showToast({
            title: '请勾选隐私政策！',
            icon: "none"
          })
        }
      } else {
        wx.showToast({
          title: '密码包含数字和字母！',
          icon: "none"
        })
      }
    } else {
      wx.showToast({
        title: '输入正确的账号！',
        icon: "none"
      })
    }
  },

   getUserInfo() {
    var userInfo = wx.getStorageSync('userInfo')
    if (userInfo) {
      this.setData({
        userInfo
      })
    } else {
      util.request_get('/web/user/personal-info', {
        userId: '22080000'
      }).then((res) => {
        if (res.code == 200) {
          // 下次报错的话  先看看你响应结果有没有成功 
          this.setData({
            userInfo: res.userInfo
          })
          // 存到全局变量
          wx.setStorageSync('userInfo', res.userInfo)
          app.globalData.userInfo = res.userInfo
          wx.switchTab({
            url: '/pages/my/my',
          })
        }
      })
    }

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})