// app.js
//app.js
import touch from './utils/touch.js' //新加

App({
  globalData: {
    userInfo: {},
    Modelmes: null,
    model: ['iPhone X', 'iPhone XR', 'iPhone XS Max', 'iPhone 11', 'iPhone 11 Pro', 'iPhone 11 Pro Max', 'iPhone 12', 'iPhone 12 Pro', 'iPhone 12 Pro Max']
  },
  touch: new touch(), //实例化这个touch对象
  onLaunch() {
    const {
      model
    } = this.globalData
    wx.getSystemInfo({ //当小程序初始化完成时 获取用户的手机机型
      success: (res) => {
        const hasMa = model.find(item => item === res.model)
        this.globalData.Modelmes = hasMa && hasMa.length > 0
      }
    })

    // 展示本地存储能力
    const logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      }
    })
  },
})