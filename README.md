# 校园快转
## 项目介绍
这个微信小程序二手购物商城的背景是为了满足用户对二手商品购买和交易的需求。随着社会的发展和环保意识的提高，越来越多的人开始重视二手商品的价值和可持续利用。然而，传统的二手交易方式存在一些问题，如信息不透明、交易不便利等。因此，开发这个小程序的目的是为了打造一个便捷、安全、可信赖的二手购物平台，为用户提供更好的购物体验。

## 项目技术
1. 原生微信小程序语言

## 项目开源地址
校园快转前端：[https://gitee.com/coderyaling/second-shop-web.git](https://gitee.com/coderyaling/second-shop-web.git)

校园快转后端：[https://gitee.com/codergjw/second-shop.git](https://gitee.com/codergjw/second-shop.git)

 _本项目将长期更新与维护，欢迎拉取和star！_ 
## 功能完善情况
[X] 登录注册
[X] 首页模块
[X] 检索模块
[X] 商品详情
[X] 商品评论
[X] 购物车
[X] 商品收藏
[X] 商品点赞
[X] 商品发布
[X] 个人中心
[X] 订单模块
[X] 订单详情
[ ] 支付模块
[X] 转让中心
[X] 店铺中心
[X] 作品上传
[X] 消息中心
[X] 地址管理
[X] 浏览记录
[ ] 订单信息填写
## 项目预览
这里展示项目的全部小程序界面预览图

![输入图片说明](images/%E7%99%BB%E5%BD%95%E4%B8%AD%E5%BF%83.png)
![输入图片说明](images/%E6%B3%A8%E5%86%8C%E4%B8%AD%E5%BF%83.png)
![输入图片说明](images/%E9%A6%96%E9%A1%B5-1.png)
![输入图片说明](images/%E9%A6%96%E9%A1%B5-2.png)
![输入图片说明](images/%E9%A6%96%E9%A1%B5-3.png)
![输入图片说明](images/%E9%A6%96%E9%A1%B5-4.png)
![输入图片说明](images/%E5%95%86%E5%93%81%E8%AF%A6%E6%83%85.png)
![输入图片说明](images/%E5%95%86%E5%93%81%E8%AF%A6%E6%83%85%E6%95%B0%E6%8D%AE.png)
![输入图片说明](images/%E5%95%86%E5%93%81%E8%AF%84%E8%AE%BA.png)
![输入图片说明](images/%E5%BA%97%E9%93%BA%E4%B8%AD%E5%BF%83.png)
![输入图片说明](images/%E4%B8%AA%E4%BA%BA%E4%B8%AD%E5%BF%83-%E6%9C%AA%E7%99%BB%E5%BD%95%E7%8A%B6%E6%80%81.png)
![输入图片说明](images/%E4%B8%AA%E4%BA%BA%E4%B8%AD%E5%BF%83-%E7%99%BB%E5%BD%95%E7%8A%B6%E6%80%81.png)
![输入图片说明](images/%E7%82%B9%E8%B5%9E%E9%A1%B5%E9%9D%A2.png)
![输入图片说明](images/%E6%94%B6%E8%97%8F%E6%95%B0%E6%8D%AE.png)
![输入图片说明](images/%E6%B5%8F%E8%A7%88%E8%AE%B0%E5%BD%95.png)
![输入图片说明](images/%E8%AE%A2%E5%8D%95%E4%B8%AD%E5%BF%83.png)
![输入图片说明](images/%E5%BE%85%E6%94%B6%E8%B4%A7.png)
![输入图片说明](images/%E8%AE%A2%E5%8D%95%E5%AE%8C%E6%88%90%E7%8A%B6%E6%80%81.png)
![输入图片说明](images/%E5%9C%B0%E5%9D%80%E7%AE%A1%E7%90%86.png)
![输入图片说明](images/%E5%9C%B0%E5%9D%80%E7%BC%96%E8%BE%91-%E6%B7%BB%E5%8A%A0.png)
![输入图片说明](%E5%8F%91%E5%B8%831.png)
![输入图片说明](%E5%8F%91%E5%B8%83-2.png)
![输入图片说明](%E5%8F%91%E5%B8%83-3.png)
![输入图片说明](%E5%8F%91%E5%B8%83-4.png)
![输入图片说明](%E5%8F%91%E5%B8%83%E4%BF%A1%E6%81%AF.png)
![输入图片说明](%E5%8F%91%E5%B8%83%E4%BD%9C%E5%93%81-%E6%97%B6%E9%97%B4%E9%80%89%E6%8B%A9.png)
![输入图片说明](%E6%89%8B%E6%9C%BA%E6%8D%A2%E7%BB%91.png)
![输入图片说明](%E6%B6%88%E6%81%AF%E4%B8%AD%E5%BF%83.png)
![输入图片说明](%E6%84%8F%E8%A7%81%E5%8F%8D%E9%A6%88.png)